from enum import Enum
import json
import requests as __req


class Coin(Enum):
    BITCOIN = 'BTCUSDT'
    ETHEREUM = 'ETHUSDT'
    BNB = 'BNBUSDT'
    FTM = 'FTMUSDT'
    TRON = 'TRXUSDT'
    ADA = 'ADAUSDT'
    # HELIUM = 'HNTUSDT'
    ONE = 'ONEUSDT'
    IOTX = 'IOTXUSDT'
    USDT = 'USDTBRL'
    BUSD = 'BUSDBRL'

    @classmethod
    def values(cls):
        return list(map(lambda c: c.value, cls))


def get_current_price(coin: Coin):
    res = __req.get(
        __get_url(),
        params={'symbol': coin.value}
    )
    data = json.loads(res.text)
    price = float(data['price'])
    return __format_price(price)


def get_current_price_all():
    res = __req.get(__get_url())
    data = json.loads(res.text)

    my_coins = Coin.values()
    prices = []

    for coin in data:
        if coin['symbol'] in my_coins:
            price = __format_price(float(coin['price']))
            prices.append({
                'symbol': coin['symbol'],
                'price': price,
            })

    return prices


def __get_url():
    return 'https://api3.binance.com/api/v3/ticker/price'


def __format_price(price: float):
    str_price = ''
    if price >= 1000:
        str_price = '{:,.2f}'.format(price)
    else:
        str_price = '{:,.4f}'.format(price)

    str_replace = str_price.replace('.', '#')
    str_replace = str_replace.replace(',', '.')
    str_replace = str_replace.replace('#', ',')

    return str_replace
