# python-i2c-lcd

### Setup

```
sudo apt-get install python-smbus i2c-tools
sudo modprobe i2c-dev
sudo modprobe i2c-bcm2708
```

add `i2c-dev` to `/etc/modules`.

### How to detect pin position

```sh
$ i2cdetect -y 1
     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00:          -- -- -- -- -- -- -- -- -- -- -- -- --
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
20: -- -- -- -- -- -- -- 27 -- -- -- -- -- -- -- --
30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
40: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
50: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
70: -- -- -- -- -- -- -- --
```

### Usage

```
python ./display.py line_1~line_2~line_3~line_4
```

or clock demo:

```
python ./datetime-test.py
```

### Based on

- https://github.com/pimatic/pimatic/issues/271
- http://www.gejanssen.com/howto/i2c_display_2004_raspberrypi/index.html
