from datetime import datetime
from lcddriver import LCD, TextPosition
from time import sleep
from coin_api import Coin, get_current_price_all

lcd = LCD()

__SLEEP = 5
CURRENCIES = []


def update_prices():
    global CURRENCIES
    dt = datetime.now().strftime('%H:%M:%S')
    print('[{}] Updating prices'.format(dt))

    CURRENCIES = get_current_price_all()


def get_label(coin: Coin, price: str):
    if coin == Coin.USDT:
        symbol = str(coin.value).replace('USDT', 'USDT/')
    elif coin == Coin.BUSD:
        symbol = str(coin.value).replace('BUSD', 'BUSD/')
    else:
        symbol = str(coin.value).replace('USDT', '')
    return '{} - {}'.format(symbol, price)


def main():
    lcd.display_string('Loading prices...', 2)
    update_prices()

    update = 0

    while True:
        for i in range(0, len(CURRENCIES), +2):
            c1 = CURRENCIES[i]
            c2 = CURRENCIES[i + 1]

            lcd.clear()
            lcd.display_string('Criptocurrencies', 1, TextPosition.CENTER)

            line_3 = get_label(Coin(c1['symbol']), c1['price'])
            line_4 = get_label(Coin(c2['symbol']), c2['price'])

            lcd.display_string(line_3, 3)
            lcd.display_string(line_4, 4)
            sleep(__SLEEP)

        if update % 5 == 0:
            lcd.display_string(line_4 + ' *', 4)
            update_prices()
            sleep(__SLEEP)

        update += 1


if __name__ == '__main__':
    try:
        main()
    except Exception as e:
        print(e)
        lcd.backlight_off()
        raise e
    except KeyboardInterrupt:
        lcd.keyboard_stop()
